# Commit Graph and Object Store

> Inspect the commit 35ac61b in the object store
> ithout using porcelaine commands, like show, log, checkout, ...

> Navigate to the file branches/index.md an show its content.

git cat-file -p 35ac61b
git cat-file -p 37a149d # tree
git cat-file -p b74bfac # tree of branches
git cat-file -p 26a3cd1a # content of file

> There is a special kind of entry in the root commit, 
> whiche line look unusual?

